//Run local host with grunt-contrib-connect
module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        //Compile scss files into minified main.css file in dist folder
        sass: {
            dist: {
                options: {
                    sourcemap: 'none'
                },
                files: [
                    {
                        expand: true,
                        cwd: 'src/scss/',
                        src: ['**/*.scss'],
                        dest: 'src/dist/css',
                        ext: '.css'
                    }
                ]
            }
        },

        //Browsersync for auto reload on save
        browserSync: {
            bsFiles: {
                src: ['src/dist/**/*', './*.html'],
            },
            options: {
                watchTask: true,
                server: './'
            }
        },

        //Use postcss to add prefixes and rename into main.css
        postcss: {
            options: {
                map: false,
                processors: [
                    require('autoprefixer')({
                        browsersList: ['last 2 versions']
                    }),
                ],
            },
            dist: {
                src: 'src/dist/css/main.css'
            }
        },

        //Clean old CSS and JS files before minification
        clean: {
            css: ['src/dist/css/*.css', '!src/dist/css/*.min.css'],
            js: ['src/dist/js/*-es6.js', '!src/dist/js/*.min.js']
        },

        //Minify CSS Files
        cssmin: {
            options: {
                keepSpecialComments: 0
            },
            target: {
                files: [{
                    expand: true,
                    cwd: 'src/dist/css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'src/dist/css',
                    ext: '.min.css'
                }]
            }
        },

        babel: {
            options: {
                sourceMap: false,
                presets: ['@babel/preset-env']
            },
            files: {
                expand: true,
                cwd: 'src/js',
                ext: '-es6.js',
                src: ['*.js', '!accordionComponent.js', '!accordionContent.js'], 
                dest: 'src/dist/js'
            }
        },

        //Uglify Javascript Files
        uglify: {
            build: {
                src: ['src/dist/js/**/*-es6.js'],
                dest: 'src/dist/js/bundle.min.js'
            }
        },

        //Set up watch command
        watch: {
            css: {
                files: 'src/scss/**/*.scss',
                tasks: ['sass', 'cssmin', 'clean:css']
            },
            js: {
                files: 'src/js/**/*.js',
                tasks: ['babel', 'uglify', 'clean:js']
            }
        }

    });

    // Load Grunt plugins
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-babel');
    grunt.loadNpmTasks('grunt-contrib-uglify-es');
    grunt.loadNpmTasks('grunt-contrib-clean');

    // Register Grunt Tasks
    grunt.registerTask('default', ['sass', 'clean:css', 'cssmin', 'babel', 'uglify', 'clean:js', 'browserSync', 'watch']);
    grunt.registerTask('build', ['sass', 'clean:css', 'cssmin', 'babel', 'uglify', 'clean:js']);
}