
//get all accordion items
const accordionItems = document.querySelectorAll('.accordion__item');
// get all accrodion content blocks
const accordionContent = document.querySelectorAll('.accordion__content');


// addclass function
const addClass = (clickTarget, plusClass) => {
    clickTarget.classList.add(plusClass);
}
// removeclass function
const removeClass = (clickTarget, remClass) => {
    clickTarget.classList.remove(remClass);
}
// toggleclass function
const toggleClass = (clickTarget, togClass) => {
    clickTarget.classList.toggle(togClass);
}

// smootscroll function
const smoothScroll = (clickTarget) => {
    let targetPos = clickTarget.parentElement.getBoundingClientRect().top;
    let startPos = window.pageYOffset;
    let distance = targetPos - startPos;
    clickTarget.scrollIntoView({
        top: targetPos + clickTarget.height,
        behavior: 'smooth'
    });
}

// Hide all except for target accordion
accordionItems.forEach((accordion) => {
    // Clicked accordions target
    const accordionTitle = accordion.firstElementChild;
    //console.log(accordion.firstElementChild);

    // Add a click to each title element to allow dropdown functionality
    accordionTitle.addEventListener('click', accordionToggle);

});

//Main function for functionality
function accordionToggle(e) {
    // Store e.target in variable target
    let target = e.target;
    //console.log(target);

    // Smoothscroll to target
    smoothScroll(target);

    accordionContent.forEach((content) => {
        //console.log(content.previousElementSibling);

        // Check if click matches previous element sibling
        if(content.previousElementSibling === target) {
            toggleClass(content, 'hidden');
            toggleClass(content.parentElement, 'active');
            content.firstElementChild.style.maxHeight = content.firstElementChild.scrollHeight + 'px'; //Adjust height for slidetoggle
        } else {
            removeClass(content.parentElement, 'active');
            setTimeout(function() {
                addClass(content, 'hidden');
            },200); //Delay the display hidden class by 0.2s
            content.firstElementChild.style.maxHeight = null; //Remove height for slidetoggle
        }

        // Toggle aria-expand when clicked for accessibility
        let ariaExpander = content.parentElement.getAttribute('aria-expanded');
        if (ariaExpander === true){
            target.setAttribute('aria-expanded', false); //Set any aria-expanded attr to False if already set to true
        } else {
            target.setAttribute('aria-expanded', true);
            content.previousElementSibling.setAttribute('aria-expanded', 'false'); //Set aria-expanded attr to false on all other siblings set to True
        }

    });
}
