

const contentTemplate = document.createElement('template');

//Increment ID of question for each question
let n = 1;
let accordionDetails = document.querySelectorAll('accordion-question');
accordionDetails.forEach(contentBlock => {
    n++;
});

contentTemplate.innerHTML = `
    <style>@import "/src/dist/css/main.min.css"</style>
    <div class="accordion__content hidden" aria-labelledby="accordion-question-${n}">
        <div class='accordion__content--inner'>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis imperdiet libero ultricies quam vestibulum, facilisis auctor eros placerat. Donec euismod sapien at dignissim consectetur. In vel orci ac massa dignissim tempor. Sed sed sem purus. In ornare justo non turpis efficitur ultrices. Donec egestas tincidunt nibh, sed lacinia ex volutpat eu. Sed tristique magna ac feugiat rhoncus. Curabitur interdum orci metus, nec aliquet purus cursus sed.</p>
        <p>In ornare justo non turpis efficitur ultrices. Donec egestas tincidunt nibh, sed lacinia ex volutpat eu. Sed tristique magna ac feugiat rhoncus. Curabitur interdum orci metus, nec aliquet purus cursus sed.In ornare justo non turpis efficitur ultrices. Donec egestas tincidunt nibh, sed lacinia ex volutpat eu. Sed tristique magna ac feugiat rhoncus. Curabitur interdum orci metus, nec aliquet purus cursus sed.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis imperdiet libero ultricies quam vestibulum, facilisis auctor eros placerat. Donec euismod sapien at dignissim consectetur. In vel orci ac massa dignissim tempor. Sed sed sem purus. In ornare justo non turpis efficitur ultrices. Donec egestas tincidunt nibh, sed lacinia ex volutpat eu. Sed tristique magna ac feugiat rhoncus. Curabitur interdum orci metus, nec aliquet purus cursus sed.</p>
        </div>
    </div>
`;

class AccordionContent extends HTMLElement {
    constructor() {
        super();

        this._shadowRoot = this.attachShadow({ mode: 'open' });
        this._shadowRoot.appendChild(contentTemplate.content.cloneNode(true));
    }
    
}

window.customElements.define('app-accordion-content', AccordionContent);