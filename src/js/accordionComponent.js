

const accordionTemplate = document.createElement('template');

//Increment ID of question for each question
let l = 1;
let accordionQuestion = document.querySelectorAll('accordion-question');
accordionQuestion.forEach(question => {
    l++;
});

accordionTemplate.innerHTML = `
    <style>@import "/src/dist/css/main.min.css"</style>
    <button id="accordion-question-${l}" class="accordion__btn" role="button" aria-expanded="false">Question ${l}
        <span class='accordion--icon'><span></span><span></span></span>
    </button>
`;

class Accordion extends HTMLElement {
    constructor() {
        super();

        this._shadowRoot = this.attachShadow({ mode: 'open' });
        this._shadowRoot.appendChild(accordionTemplate.content.cloneNode(true));
    }
    
}

window.customElements.define('app-accordion', Accordion);